FROM mcr.microsoft.com/dotnet/sdk:6.0

RUN curl -fsSL https://deb.nodesource.com/setup_19.x | bash - && apt-get install -y nodejs
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN dotnet build
WORKDIR /app/DotnetTemplate.Web
RUN npm install
RUN npm run build
RUN chown -R root:root ./node_modules/escodegen/node_modules/optionator
CMD ["dotnet", "run"]